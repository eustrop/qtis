-- 2. Инициализация БД (выполняет владелец БД, пользователь tis)
CREATE SCHEMA tis;
GRANT USAGE ON SCHEMA tis TO PUBLIC;
CREATE TABLE tis.SAMUsers ( id int, db_user name, name char(16),
CONSTRAINT SAMUsers_pkey PRIMARY KEY (id) );
CREATE UNIQUE INDEX SAMUsers_idx_db_user on tis.SAMUsers(db_user);
CREATE TABLE tis.SAMACL ( user_id int, scope_id int,
read_allowed char(1), write_allowed char(1),
CONSTRAINT SAMACL_pkey PRIMARY KEY (user_id, scope_id) );
CREATE FUNCTION tis.sam_get_user() RETURNS integer STABLE
AS 'select id from tis.SAMUsers where db_user = session_user'
LANGUAGE SQL SECURITY DEFINER;
-- 2.1 Создание таблицы для хранения прикладных данных
CREATE TABLE tis.Somedata (id int not null, scope_id int not null,
user_id int not null, value varchar(32),
CONSTRAINT Somedata_pkey PRIMARY KEY (id));
CREATE SEQUENCE tis.seq_Somedata;
