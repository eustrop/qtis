-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- purpose: drop all dbs and users from cluster

DROP DATABASE qrtisdb;
drop USER qtis_dbo;
select 'qtis_users ROLE can be used by other ConcepTIS derived systems, drop it manually if you need'
"WARNING: on noDROP ROLE qtis_users";

