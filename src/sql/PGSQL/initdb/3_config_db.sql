-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--

-- 2. configure ConcepTIS database. must be done by superuser at ConcepTISDB
-- 2.1. disabling usage of "public" schema complitely
-- REVOKE CREATE ON SCHEMA public FROM PUBLIC;
-- REVOKE USAGE ON SCHEMA public FROM PUBLIC;
-- 2.2 ... or just drop it at all.
DROP SCHEMA public;

