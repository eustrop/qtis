-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- purpose: create DBMS users needed for system initialising and testing
--	qtis_dbo - owner of all DB objects (tables,views,functions)
--	qtis_users - group role for joining all ConcepTIS users
--
-- qtisadmin  - default administrator of this installation
-- qtisoperator  - default operator of this installation
-- qtisreplicator  - default user for loading data from other installations
-- qtiswww  - default user for www server to host sites from this DB
-- qtisguest  - guest user for any unidentified public user
-- qtisnobody  - user with no access to data (for testing purpose at least)
--
--	qtisuser1,qtisuser2,... - accounts for regular users (dynamically assigned)
--	
--

-- 1. creating users. must be done at postegres db, by superuser
CREATE USER qtis_dbo LOGIN INHERIT;
CREATE ROLE qtis_users NOLOGIN NOINHERIT;
--
CREATE USER qtisadmin LOGIN INHERIT IN ROLE qtis_users;
CREATE USER qtisoperator LOGIN INHERIT IN ROLE qtis_users;
CREATE USER qtisreplicator LOGIN INHERIT IN ROLE qtis_users;
CREATE USER qtiswww LOGIN INHERIT IN ROLE qtis_users;
CREATE USER qtisguest LOGIN INHERIT IN ROLE qtis_users;
CREATE USER qtisnobody LOGIN INHERIT IN ROLE qtis_users;
--
CREATE USER qtisuser1 LOGIN INHERIT IN ROLE qtis_users;
CREATE USER qtisuser2 LOGIN INHERIT IN ROLE qtis_users;
CREATE USER qtisuser3 LOGIN INHERIT IN ROLE qtis_users;

