-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--

-- 3. initial configuration of qtisdb. must be done by "qtis_dbo"
--    user which is owner of that database

-- 3.2. deny connection to this DB anyone except qtis_users
REVOKE ALL ON DATABASE qrtisdb FROM PUBLIC;
GRANT CONNECT ON DATABASE qrtisdb TO qtis_users;

-- 3.3. create separate schemas for dictionary (dic), Security Access
--      Management (SAM), general-purpose TIS/SQL objects and
--	functions (TIS).
--	Application-specific ConcepTIS objects placed at separate
--	schema (TISC).

CREATE SCHEMA dic;
GRANT USAGE ON SCHEMA dic TO PUBLIC;

CREATE SCHEMA sam;
GRANT USAGE ON SCHEMA sam TO PUBLIC;

CREATE SCHEMA tis;
GRANT USAGE ON SCHEMA tis TO PUBLIC;

--CREATE SCHEMA tisc;
--GRANT USAGE ON SCHEMA tisc TO PUBLIC;

CREATE SCHEMA notes;
GRANT USAGE ON SCHEMA notes TO PUBLIC;

--
-- Это схемы EustroERP для DOMINATORCO.RU
-- Но кое-что должно попасть обратно в ConcepTIS
--
CREATE SCHEMA otab;
GRANT USAGE ON SCHEMA otab TO PUBLIC;
CREATE SCHEMA rngs;
GRANT USAGE ON SCHEMA rngs TO PUBLIC;
CREATE SCHEMA fs;
GRANT USAGE ON SCHEMA fs TO PUBLIC;
CREATE SCHEMA calc;
GRANT USAGE ON SCHEMA calc TO PUBLIC;
CREATE SCHEMA wiki;
GRANT USAGE ON SCHEMA wiki TO PUBLIC;
CREATE SCHEMA qrdb;
GRANT USAGE ON SCHEMA qrdb TO PUBLIC;
CREATE SCHEMA cop;
GRANT USAGE ON SCHEMA cop TO PUBLIC;
CREATE SCHEMA bill;
GRANT USAGE ON SCHEMA bill TO PUBLIC;
--CREATE SCHEMA yml;
--GRANT USAGE ON SCHEMA yml TO PUBLIC;

-- 3.4. install PLPgSQL.
--      this could be used at old versions of PostgeSQL:
-- CREATE FUNCTION plpgsql_call_handler() RETURNS language_handler AS
--    '$libdir/plpgsql' LANGUAGE C;
-- CREATE FUNCTION plpgsql_validator(oid) RETURNS void AS
--  '$libdir/plpgsql' LANGUAGE C;
-- CREATE TRUSTED PROCEDURAL LANGUAGE plpgsql
--   HANDLER plpgsql_call_handler VALIDATOR plpgsql_validator;

CREATE PROCEDURAL LANGUAGE plpgsql;
