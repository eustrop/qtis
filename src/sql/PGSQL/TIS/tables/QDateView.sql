-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- $Id$
--

--DROP TABLE IF EXISTS TIS.QDateView CASCADE;
CREATE TABLE TIS.QDateView (
	uid	bigint NOT NULL, -- id of owner of this record
	ts	timestamp NOT NULL, -- when this record installed
	QDATO	timestamp NULL, -- date slice to look over the VD_ views 
	PRIMARY KEY (uid)
	);
