-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE at the project's root directory
--
-- $Id$
--

-- DROP TABLE IF EXISTS TIS.QObject CASCADE;
CREATE TABLE TIS.QObject (
	QOID	bigint NOT NULL,
	QVER	bigint NOT NULL,
	QSYS	varchar(10) NOT NULL,
	QTYPE	varchar(3) NOT NULL,
	QSID	bigint NOT NULL,
	QLVL	smallint NOT NULL,
	QUID	bigint NOT NULL,
	QRID	bigint NOT NULL,
	QSTA	char(1) NOT NULL,
	QDATE	timestamp NOT NULL,
	QDATO	timestamp NULL,
	PRIMARY KEY (QOID,QVER)
	);
CREATE INDEX QObject_idx1 on TIS.QObject(QDATE,QDATO);
