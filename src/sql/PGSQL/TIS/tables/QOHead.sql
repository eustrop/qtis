-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
--    see LICENSE.BALES file at the root of project or bales.eustrosoft.org
--

-- DROP TABLE IF EXISTS TIS.QOHead CASCADE;
CREATE TABLE TIS.QOHead (
	QOID	bigint NOT NULL,
	QRID	bigint NOT NULL,
	QVER	bigint NOT NULL,
	QTOV	bigint NOT NULL,
	QSID	bigint NOT NULL,
	QLVL	smallint NOT NULL,
	QPID	bigint NOT NULL,
	QSYS	varchar(10) NOT NULL,
	QTYPE	varchar(3) NOT NULL,
	name	varchar(255),
	type	varchar(16),
	descr	varchar(1024),
	content	text,
	comment	text,
	PRIMARY KEY (QOID,QRID,QVER)
	);
