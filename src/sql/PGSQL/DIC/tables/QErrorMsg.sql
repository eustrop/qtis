-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--
-- dictionary for error an informational messages with it's  correspondence
-- numeric (QErrorMsg.num) and symbolic (QErrorMsg.code) codes. Special field
-- QErrorMsg.lang can be used for localization and internationalization.

-- DROP TABLE IF EXISTS dic.QErrorMsg CASCADE;
CREATE TABLE dic.QErrorMsg (
	errnum	smallint NOT NULL,
	errcode	char(16) NOT NULL,
	lang	char(3) NOT NULL,
	errmsg	varchar(1024) NULL,
	PRIMARY KEY (errnum,lang),
	UNIQUE (errcode,lang)
	);
