-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--
-- main dictionary table (dic.QCode) correspondence view named "dic.V_QDic"

-- DROP TABLE IF EXISTS dic.QCode CASCADE;
CREATE TABLE dic.QCode (
	dic	char(32) NOT NULL,
	code	char(16) NOT NULL,
	value	varchar(128) NOT NULL,
	descr	varchar(1024) NULL,
	PRIMARY KEY (dic,code)
	);
