-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--

-- dictionary filling assistance. db admin use only
CREATE OR REPLACE FUNCTION dic.set_code(
   v_dic char(32), v_code char(16), v_value varchar(128), v_descr varchar(1024)
	) RETURNS VOID LANGUAGE plpgSQL SECURITY DEFINER AS $$
  BEGIN
  	update dic.QCode as QC set value = v_value, descr = v_descr
	where QC.dic = v_dic and QC.code = v_code;
	IF NOT FOUND THEN
	 insert into dic.QCode values($1,$2,$3,$4);
	END IF;
  END $$;

