-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--
--

BEGIN TRANSACTION;
-- dictionary filling assistance. db admin use only
CREATE OR REPLACE FUNCTION dic.set_code(
   dic char(16), code char(16), value varchar(99), descr varchar(255)
	) RETURNS VOID 
  AS ' delete from dic.QCode QC where QC.dic = $1 and QC.code = $2;
	insert into dic.QCode values($1,$2,$3,$4);'
    LANGUAGE SQL SECURITY DEFINER;
REVOKE ALL ON FUNCTION dic.set_code( dic char(16), code char(16),
	value varchar(99), descr varchar(255) ) FROM PUBLIC;

CREATE OR REPLACE FUNCTION dic.drop_dic(dic char(16)) RETURNS VOID 
  AS ' delete from dic.QCode QC where QC.dic = $1;'
    LANGUAGE SQL SECURITY DEFINER;
REVOKE ALL ON FUNCTION dic.drop_dic(dic char(16)) FROM PUBLIC;

CREATE OR REPLACE FUNCTION dic.drop_all_dic() RETURNS VOID 
  AS ' delete from dic.QCode;'
    LANGUAGE SQL SECURITY DEFINER;
REVOKE ALL ON FUNCTION dic.drop_all_dic() FROM PUBLIC;

-- dictionary localized messages filling assistance. db admin use only
CREATE OR REPLACE FUNCTION dic.set_code_msg(
	dic char(16), code char(16), lang char(2), value varchar(99),
	descr varchar(255)
	) RETURNS VOID 
  AS ' delete from dic.QCodeMsg QC where QC.dic = $1 and QC.code = $2;
	insert into dic.QCodeMsg values($1,$2,$3,$4,$5);'
    LANGUAGE SQL SECURITY DEFINER;
REVOKE ALL ON FUNCTION dic.set_code_msg(
        dic char(16), code char(16), lang char(2), value varchar(99),
	descr varchar(255) ) FROM PUBLIC;


-- errors definitions dictionary filling assistance. db admin use only
CREATE OR REPLACE FUNCTION dic.set_ErrorMsg(
	v_errnum integer, v_errcode char(16), v_lang char(2), v_errmsg varchar(255)
	) RETURNS VOID LANGUAGE plpgSQL SECURITY DEFINER AS $$
BEGIN
	update dic.QErrorMsg as QEM set errnum=v_errnum, errcode = v_errcode,
	lang = v_lang, errmsg = v_errmsg
	where QEM.errnum = v_errnum;
	IF NOT FOUND THEN
	 insert into dic.QErrorMsg values($1,$2,$3,$4);
	END IF;
	IF v_lang = 'EN' THEN
	 PERFORM dic.set_code('ERRORS',v_errcode,v_errcode,v_errmsg);
	END IF;
END $$;
REVOKE ALL ON FUNCTION dic.set_ErrorMsg( v_errnum integer,
	v_errcode char(16), v_lang char(2),
	v_errmsg varchar(255) ) FROM PUBLIC;
COMMIT TRANSACTION;
