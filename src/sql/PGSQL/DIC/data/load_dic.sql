-- QTIS project
--
-- (c) Alex V Eustrop & EustroSoft.org 2022
-- project's home: qtis.eustrosoft.org
--
-- LICENSE (whole project): see LICENSE.QTIS file at root directory of project
-- LICENSE (this file): BALES, BSD or MIT on your choice.
-- LICENSE (this file): see LICENSE.BALES file at the root of this project
-- LICENSE (this file): see bales.eustrosoft.org if no LICENSE.BALES file found
--
-- this file based on code from
-- ConcepTIS project
-- (c) Alex V Eustrop 2009
-- see LICENSE.ConcepTIS at the project's root directory
--
-- purpose: load data into main dictionary (dic.QCode)

select dic.drop_all_dic();
select dic.set_code('YESNO','Y','YES/TRUE',null);
select dic.set_code('YESNO','N','NO/FALSE',null);
select dic.set_code('YESNOUNK','Y','YES/TRUE',null);
select dic.set_code('YESNOUNK','N','NO/FALSE',null);
select dic.set_code('YESNOUNK','?','UNKNOWN',null);
select dic.set_code('LANG','EN','English',null);
select dic.set_code('LANG','RU','Russian',null);
select dic.set_code('LANG','UK','Ukrainian',null);
select dic.set_code('LANG','FR','French',null);
select dic.set_code('LANG','DE','German',null);
select dic.set_code('LANG','ES','Español',null);
select dic.set_code('LANG','PT','Português',null);
select dic.set_code('LANG','EO','Esperanto',null);
select dic.set_code('LANG','NL','Nederlands',null);
select dic.set_code('LANG','EL','Hellenic',null);
select dic.set_code('LANG','ZH','汉语',null);
select dic.set_code('LANG','TW','漢語',null);
select dic.set_code('LANG','JP','日本語',null);
select dic.set_code('SLEVEL','0','unclassified',null);
select dic.set_code('SLEVEL','10','restricted',null);
select dic.set_code('SLEVEL','20','classified',null);
select dic.set_code('SLEVEL','30','top secret',null);
select dic.set_code('SLEVEL','40','more then top secret',null);
select dic.set_code('CAPABILITY','SAM_MANAGE','SECURITY RISK! manage any SAM objects',null);
select dic.set_code('CAPABILITY','SAM_DBOWNER','this user able to use DB passing over TIS',null);
select dic.set_code('CAPABILITY','SAM_VIEWAUDITLOG','this user able to view SAM.AuditEvent data',null);
select dic.set_code('CAPABILITY','SLVL_MANAGE','change SLEVEL of objects',null);
select dic.set_code('CAPABILITY','SLVL_DECREASE','decrease SLEVEL of objects',null);
select dic.set_code('CAPABILITY','FORCE_ROLLBACK','rollback logical transactions owned by others',null);
select dic.set_code('CAPABILITY','MOVE_SEND','send data object to another scope',null);
select dic.set_code('CAPABILITY','MOVE_RECEIVE','receive data object from "transit" state',null);
select dic.set_code('CAPABILITY','V0_BYPASS_ACL','SECURITY BYPASS! view data over V0_ (since 2020-12-04)',null);
select dic.set_code('EVENT_CLASS','0','system, unmaskable',null);
select dic.set_code('EVENT_CLASS','1','data access, unmaskable',null);
select dic.set_code('EVENT_CLASS','2','data access, maskable',null);
select dic.set_code('EVENT_CLASS','3','warning, maskable',null);
select dic.set_code('EVENT_CLASS','4','debug, maskable',null);
select dic.set_code('EVENT_ACTION','C','create',null);
select dic.set_code('EVENT_ACTION','U','update',null);
select dic.set_code('EVENT_ACTION','F','field changed',null);
select dic.set_code('EVENT_ACTION','D','delete',null);
select dic.set_code('EVENT_ACTION','V','read',null);
select dic.set_code('EVENT_ACTION','H','history read',null);
select dic.set_code('EVENT_ACTION','X','execute',null);
select dic.set_code('EVENT_ACTION','XC','capability used',null);
select dic.set_code('EVENT_ACTION','R','rollback',null);
select dic.set_code('EVENT_ACTION','M','metadata',null);
select dic.set_code('EVENT_ACTION','UD','undelete',null);
select dic.set_code('EVENT_ACTION','N','rename',null);
select dic.set_code('EVENT_ACTION','L','lock',null);
select dic.set_code('EVENT_ACTION','UL','unlock',null);
select dic.set_code('EVENT_ACTION','TS','send',null);
select dic.set_code('EVENT_ACTION','TT','send to',null);
select dic.set_code('EVENT_ACTION','TR','receive',null);
select dic.set_code('EVENT_ACTION','SR','slevel request',null);
select dic.set_code('EVENT_ACTION','SI','slevel increase',null);
select dic.set_code('EVENT_ACTION','SD','slevel decrease',null);
select dic.set_code('EVENT_ACTION','OO','object open',null);
select dic.set_code('EVENT_ACTION','OC','object commit',null);
select dic.set_code('EVENT_ACTION','OR','object rollback',null);
select dic.set_code('EVENT_ACTION','OM','object move',null);
select dic.set_code('EVENT_ACTION','AW','access write',null);
select dic.set_code('EVENT_ACTION','AC','access create',null);
select dic.set_code('EVENT_ACTION','AD','access delete',null);
select dic.set_code('EVENT_ACTION','AR','access read',null);
select dic.set_code('EVENT_ACTION','AL','access lock',null);
select dic.set_code('EVENT_ACTION','AH','access history',null);
select dic.set_code('ACCESS_ACTION','W','write',null);
select dic.set_code('ACCESS_ACTION','C','create',null);
select dic.set_code('ACCESS_ACTION','D','delete',null);
select dic.set_code('ACCESS_ACTION','R','read',null);
select dic.set_code('ACCESS_ACTION','H','history read',null);
select dic.set_code('ACCESS_ACTION','L','lock',null);
select dic.set_code('ZSTA','N','New','Current version');
select dic.set_code('ZSTA','C','Corrected','corrected version of object');
select dic.set_code('ZSTA','R','Rollback','(reserved) rolled back');
select dic.set_code('ZSTA','P','Prepared','(reserved) prepared for commit, QC passed');
select dic.set_code('ZSTA','D','Deleted','Deleted object');
select dic.set_code('ZSTA','U','Undeleted','Undeleted object');
select dic.set_code('ZSTA','I','Intermediate','Incomplete logical transaction');
select dic.set_code('ZSTA','L','Locked','Object is locked');
select dic.set_code('ZSTA','F','Free','(reserved) object been freed after locking');
select dic.set_code('ZSTA','T','Transit','object in transit state');
select dic.set_code('ZSTA','A','Accepted','(reserved) Accepted from transit');
select dic.set_code('ZSTA','W','Withdrawn','(reserved)Withdrawn from transit');
select dic.set_code('ZSTA','M','Moved','Object has been moved or it''s ZLVL changed');
select dic.set_code('TIS_OBJECT_TYPE','A','Area','Area of responsibility');
select dic.set_code('TIS_OBJECT_TYPE','C','Container','Container with various objects');
select dic.set_code('TIS_OBJECT_TYPE','D','Document','Some documental object');
select dic.set_code('TIS_OBJECT_TYPE','X','SAM','SAM subsystem tables prefix');
select dic.set_code('TIS_OBJECT_TYPE','Y','YETANOTHER','Prefix for multicharacter codes of objects types');
select dic.set_code('TIS_OBJECT_TYPE','Z','SYSTEM','SYSTEM tables prefix');
select dic.set_code('ACL_OBJECT_TYPE','A','Area','Area of responsibility');
select dic.set_code('ACL_OBJECT_TYPE','C','Container','Container with various objects');
select dic.set_code('ACL_OBJECT_TYPE','D','Document','Some documental object');
select dic.set_code('ACLA','Y','Allowed','access allowed (if not rejected)');
select dic.set_code('ACLA','N','Not Allowed','access not allowed (but not rejected)');
select dic.set_code('ACLA','R','Rejected','access rejected (even if allowed by another ACL)');
select dic.set_code('DD02','MANU','manuscript',null);
select dic.set_code('DD02','ARTL','article',null);
select dic.set_code('DD02','BOOK','book',null);
select dic.set_code('DD02','DISS','dissertation',null);
select dic.set_code('DD02','ENC','encyclopedia',null);
select dic.set_code('DD02','REF','reference book',null);
select dic.set_code('DD02','LIST','list (catalogue)',null);
select dic.set_code('DD02','ANNX','annex',null);
select dic.set_code('DD02','SITE','site (Internet)',null);
select dic.set_code('DR02','U','use (cite)',null);
select dic.set_code('DR02','R','replace',null);
select dic.set_code('DR02','A','add to (supply)',null);
select dic.set_code('DR02','I','include',null);
select dic.set_code('DP01','NUM','number','any numeric value');
select dic.set_code('DP01','TEXT','text','any textual value');
select dic.set_code('DP01','MIX','mixed','mixed value (both NUM & TEXT parts)');
select dic.set_code('CC02','STOR','container storage','must contain containers only');
select dic.set_code('CC02','FILE','file for documents','must contain documents only');
select dic.set_code('CC02','MIX','mixed storage','can contain any type of objects');
-- useful SQL requests
select dic.set_code('SQL','0000000000000000','default SQL ping query','select pg_backend_pid();');
--
select dic.set_code('TIS_FIELD','XU01','User id','Unique identifier of User');
select dic.set_code('TIS_FIELD','XU02','Scope','Scope id where this user definition located');
select dic.set_code('TIS_FIELD','XU03','Access level','Secrecy access level of this person');
select dic.set_code('TIS_FIELD','XU04','Disabled','This person unallowed to access system at present time');
select dic.set_code('TIS_FIELD','XU05','Language','Default language that this user prefer to obtain messages at');
select dic.set_code('TIS_FIELD','XU06','Login','Unique login name of this user');
select dic.set_code('TIS_FIELD','XU07','DB Username','DB user that this TIS user corresponding to ');
select dic.set_code('TIS_FIELD','XU08','Full name','Full name of this person or textual specification of robot');

--
select dic.set_ErrorMsg(-4,'I_DEBUG','EN','%1');
select dic.set_ErrorMsg(-3,'I_WARNING','EN','%1');
select dic.set_ErrorMsg(-2,'I_INFO2','EN','%1');
select dic.set_ErrorMsg(-1,'I_INFO','EN','%1');
--lect dic.set_ErrorMsg(0,'1234567890123456','EN','');
select dic.set_ErrorMsg(0,'I_SUCCESS','EN','success access');
select dic.set_ErrorMsg(1,'E_NOTIMPLEMENTED','EN','not implemented');
select dic.set_ErrorMsg(2,'E_UNKNOWN','EN','unknown error occured : %1');
select dic.set_ErrorMsg(3,'E_SQL','EN','SQL error %1 : %2');
select dic.set_ErrorMsg(4,'E_TRANSISOLATION','EN','Invalid transaction isolation "%1". "%2" required');
select dic.set_ErrorMsg(5,'E_OBJECTCHANED','EN','data object %1 changed since version %2 (current version=%3)');
select dic.set_ErrorMsg(6,'E_OBJECTLOCKED','EN','data object locked');
select dic.set_ErrorMsg(7,'E_NOCHANGES','EN','no changes done');
--lect dic.set_ErrorMsg(10,'1234567890123456','EN','');
select dic.set_ErrorMsg(11,'E_NOACCESS','EN','no access');
select dic.set_ErrorMsg(12,'E_ACCESSDENIED','EN','access denied by %1');
select dic.set_ErrorMsg(13,'E_NOCAPABILITY','EN','no required capability %1 for scope %2');
select dic.set_ErrorMsg(14,'E_SLVL_LOW','EN','you secrecy level (%1) too low.');
select dic.set_ErrorMsg(15,'E_SLVL_HIGH','EN','you secrecy level (%1) too high.');
select dic.set_ErrorMsg(16,'E_NOUSER','EN','there are no current user for you (DBMS user is %1)');
select dic.set_ErrorMsg(17,'E_USERLOCKED','EN','account of user %1 is locked');
select dic.set_ErrorMsg(18,'E_USERNOTGRANTED','EN','you have not permission to use account %1');
--
select dic.set_ErrorMsg(31,'E_NORECORD','EN','record "%1" with %2="%3" does not exists');
select dic.set_ErrorMsg(32,'E_NOBJECT','EN','object "%1" with %2="%3" does not exists');
select dic.set_ErrorMsg(33,'E_NOSCOPE','EN','scope %1 does not exists');
select dic.set_ErrorMsg(34,'E_NOCODE','EN','there are no code "%1" at dictionary "%2"');
select dic.set_ErrorMsg(35,'E_NOTOPENED','EN','object %1 does not opened');
select dic.set_ErrorMsg(36,'E_NOTOWNER','EN','you are not owner of %1');
select dic.set_ErrorMsg(37,'E_NOTLOCKED','EN','object %1 does not locked');
--
select dic.set_ErrorMsg(41,'E_DUPRECORD','EN','record "%1" with %2="%3" already exists');
select dic.set_ErrorMsg(42,'E_DUPOBJECT','EN','object "%1" with %2="%3" already exists');
select dic.set_ErrorMsg(43,'E_2MANYRECORDS','EN','Too many records %1 for %2 (maximum %3 exceeded)');
select dic.set_ErrorMsg(44,'E_2MANYOBJECTS','EN','Too many objects %1 for scope %2 (maximum %3 exceeded)');
select dic.set_ErrorMsg(45,'E_NOTENOUGHRECS','EN','not enough records %1 for %2 (at least %3 required)');
--lect dic.set_ErrorMsg(10,'1234567890123456','EN','');
select dic.set_ErrorMsg(50,'E_NOTNULL','EN','unallowed null value for field "%2" of "%1"');
select dic.set_ErrorMsg(51,'E_INVALIDVALUE','EN','invalid value "%1" for field "%2"');
select dic.set_ErrorMsg(52,'E_INVALIDCODE','EN','invalid code value "%1" for field "%2" (dictionary %3)');
select dic.set_ErrorMsg(53,'E_INVALIDCHPTS','EN','invalid date %2 for scope %1 (conflict with CHPTS=%3)');
select dic.set_ErrorMsg(54,'E_INVALIDZTYPE','EN','invalid object type code "%1"');
select dic.set_ErrorMsg(55,'E_INVALIDOBJECT','EN','invalid object ZTYPE=%1 ZOID=%2');
select dic.set_ErrorMsg(56,'E_INVALIDVERSION','EN','invalid object version ZTYPE=%1 ZOID=%2 ZVER=%3');
select dic.set_ErrorMsg(57,'E_INVALIDVERDATE','EN','specified version has invalid ZDATE=%1');
select dic.set_ErrorMsg(58,'E_INVALIDRECORD','EN','invalid record %1 for object %2');
select dic.set_ErrorMsg(58,'E_INVALIDPARENT','EN','invalid ZPID = %1 for record %2 ZRID = %3');
--lect dic.set_ErrorMsg(10,'1234567890123456','EN','');
select dic.set_ErrorMsg(61,'E_NONSOLERECORD','EN','there are more then one record "%1" with %2="%3" exists');
select dic.set_ErrorMsg(62,'E_NONSOLEOBJECT','EN','there are more then one object "%1" with %2="%3" exists');
select dic.set_ErrorMsg(63,'E_NONLOCALSCOPE','EN','non local scope sid=%1');
--lect dic.set_ErrorMsg(10,'1234567890123456','EN','');
select dic.set_ErrorMsg(71,'E_NARECORD4OBJ','EN','not allowed record %1 for object %2');
select dic.set_ErrorMsg(72,'E_NAZTYPE4SCOPE','EN','not allowed object %1 for scope %2');
select dic.set_ErrorMsg(73,'E_NASYSZTYPE','EN','not allowed system object object type %1 for regular operation');
